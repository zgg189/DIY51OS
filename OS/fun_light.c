
/*包含头文件*/
#include <reg52.h>
#include "type.h"
#include "DiyOS.h"


//管脚重定义
sbit LED1 = P2^0;
sbit LED2 = P2^1;
sbit LED3 = P2^2;


//不精准的延时函数
void delay(u16 us)
{
	while(--us);
}	

//灯 1 功能函数
void LED1_Ctrl(void)
{
	int i = 0;
	while(1)
	{
		for(i = 0; i< 6; i++)
		{
			LED1 = 1;
//			delay(10000);
			os_delayms(100);
			LED1 = 0;
//			delay(10000);
			os_delayms(100);
			LED1 = 1;
		}
		os_delayms(100);
		
//		os_task_sch(1);
	}
}

//灯 2 功能函数
void LED2_Ctrl(void)
{
	int i = 0;
	while(1)
	{
		for(i = 0; i< 4; i++)
		{
			LED2 = 1;
//			delay(10000);
			os_delayms(200);
			LED2 = 0;
//			delay(10000);
			os_delayms(200);
			LED2 = 1;
		}
//		os_task_sch(2);
	}
}

//灯 3 功能函数
void LED3_Ctrl(void)
{
	int i = 0;
	while(1)
	{
		for(i = 0; i< 2; i++)
		{
			LED3 = 1;
			delay(10000);
//			os_delayms(100);
			LED3 = 0;
			delay(10000);
//			os_delayms(100);
			LED3 = 1;
 		}
//		os_task_sch(0);		
//		for(i = 0; i< 1; i++)
//		{
//			LED3 = 1;
//			delay(10000);
//			LED3 = 0;
//			delay(10000);
//			LED3 = 1;		
//		}
//		os_task_sch(0);
	}
}



//初始化函数
void IO_Init(void)
{
	LED1 = 1;
	LED2 = 1;
	LED3 = 1;
}

//任务栈初始化函数
void task_init(void)
{	
	os_task_add(0,LED1_Ctrl);
	os_task_add(1,LED2_Ctrl);
	os_task_add(2,LED3_Ctrl);
}

//主函数
void main(void)
{
	//初始化函数部分
	IO_Init();
	Time0_Init();
	os_init();
	task_init();
	
	os_task_start(0);
	
}




