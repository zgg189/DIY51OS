#ifndef __DIYOSCONFIG_H__
#define __DIYOSCONFIG_H__

#include <reg52.h>

#define TASK_STACK_SIZE	30  //任务栈空间
#define TASK_MAX_NUM	4  //任务的最大数量
#define TASK_IDLE_NUM	(TASK_MAX_NUM - 1) //最后一个任务作为空闲任务
#define TASK_VALID_NUM	(TASK_MAX_NUM - 2) //有效的最大任务号

#endif
