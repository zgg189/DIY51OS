#include "DiyOS.h"
#include "DiyOSConfig.h"

DIYTASK_INFO idata task_info[TASK_MAX_NUM];

/*****************************系统信息部分***********************************/
//定义系统信息结构体
DIYOS_INFO idata os_info;

void OS_SetCurTask(u8 curtask)
{
	os_info.cur_task = curtask;
}

/*****************************/
/***获取当前执行的任务号****/
/*****************************/
u8 OS_GetCurTask(void)
{
	return os_info.cur_task;
}

/*****************************/
/***寻找下一个要执行的任务号**/
/*****************************/
u8 OS_GetNextTask(void)
{
	static u8 task_tmp = 0;
	task_tmp = os_info.cur_task;
	while(1)
	{
		if(task_tmp < TASK_VALID_NUM) //设定最后一个任务为空闲任务，所以有效任务就是前面的任务个数
		{
			task_tmp +=1;
		}
		else 
			task_tmp = 0;
		
		if(task_tmp == os_info.cur_task) { os_info.cur_task = TASK_IDLE_NUM;break; }
		if(task_info[task_tmp].status == READY_STATUS) {os_info.cur_task = task_tmp; break;}
		else continue;
	}
	
	return os_info.cur_task;
}


/*********************任务处理部分*******************************************/

//任务结构体初始化
void os_task_init(void)
{
	u8 i = 0;
	for(i = 0 ; i < TASK_MAX_NUM; i++)
	{
		memset(&task_info[i],0,sizeof(DIYTASK_INFO));
	}
}

//将任务添加到系统中
void os_task_add(u8 task_num,void (*task_fun)(void))
{
	task_info[task_num].addr_tab[1] = (u16)task_fun &0xff;
	task_info[task_num].addr_tab[2] = ((u16)task_fun >> 8)&0xff;
	task_info[task_num].stack_top 	= (task_info[task_num].addr_tab+15);
	task_info[task_num].delayms		= 0;
	task_info[task_num].status		= READY_STATUS;
	task_info[task_num].num 		= task_num;
}

//输出栈指针
u8 os_task_outSp(u8 task_num)
{
	return task_info[task_num].stack_top;
}


/***********************任务调度部分***********************************/

//任务调度函数
//void os_task_sch(u8 task_next_index)
void os_task_sch(void)
{
	EA = 0;
	/*
	__asm PUSH	ACC   ;压栈
	__asm PUSH	B
	__asm PUSH	PSW
	__asm PUSH	AR0
	__asm PUSH	AR1
	__asm PUSH	AR2
	__asm PUSH	AR3
	__asm PUSH	AR4
	__asm PUSH	AR5
	__asm PUSH	AR6
	__asm PUSH	AR7
	*/
	
	__asm PUSH	ACC   ;压栈
	__asm PUSH	B
	__asm PUSH	DPH
	__asm PUSH	DPL
	__asm PUSH	PSW
	__asm PUSH	AR0
	__asm PUSH	AR1
	__asm PUSH	AR2
	__asm PUSH	AR3
	__asm PUSH	AR4
	__asm PUSH	AR5
	__asm PUSH	AR6
	__asm PUSH	AR7	
	
	

	
	task_info[OS_GetCurTask()].stack_top = SP;
//	OS_SetCurTask(task_next_index);
//	SP = task_info[OS_GetCurTask()].stack_top;
	task_info[OS_GetNextTask()].status = RUN_STATUS;
	SP = task_info[OS_GetCurTask()].stack_top;
	
	
	__asm POP	AR7	;出栈
	__asm POP	AR6
	__asm POP	AR5
	__asm POP	AR4
	__asm POP	AR3
	__asm POP	AR2
	__asm POP	AR1
	__asm POP	AR0
	__asm POP	PSW
	__asm POP	DPL
	__asm POP	DPH
	__asm POP	B
	__asm POP	ACC		
	
	/*
	__asm POP	AR7	;出栈
	__asm POP	AR6
	__asm POP	AR5
	__asm POP	AR4
	__asm POP	AR3
	__asm POP	AR2
	__asm POP	AR1
	__asm POP	AR0
	__asm POP	PSW
	__asm POP	B
	__asm POP	ACC	
	*/
	
	EA = 1;
}

//任务启动
//以后的开始任务从这个任务开始创建
void os_task_start(u8 task_num)
{
	task_info[task_num].status = RUN_STATUS;
	SP = os_task_outSp(task_num);
	OS_SetCurTask(task_num);
	__asm POP	AR7	;出栈
	__asm POP	AR6
	__asm POP	AR5
	__asm POP	AR4
	__asm POP	AR3
	__asm POP	AR2
	__asm POP	AR1
	__asm POP	AR0
	__asm POP	PSW
	__asm POP	DPL
	__asm POP	DPH
	__asm POP	B
	__asm POP	ACC
}

/*************************  延时任务切换 *******************************/
void os_task_delayms_handle(void)
{
	static u8 i = 0;
	for(i = 0; i < (TASK_VALID_NUM+1);i++)
	{
		if(task_info[i].status == DELAY_STATUS)
		{
			task_info[i].delayms--;
			if(task_info[i].delayms == 0) 
				task_info[i].status = READY_STATUS;
		}
	}
}

void os_delayms(u16 ms)
{
	EA = 0;//关中断
	task_info[os_info.cur_task].delayms = ms;
	task_info[os_info.cur_task].status = DELAY_STATUS;
	os_task_sch();
	EA = 1;//开中断
}

/**********************定时器部分************************************/
void Time0_Init(void)
{
	TMOD = 0x01;
	/*晶振是 11.0592MHz，定时时间为：10ms TH0 = 0xDC  TL0 = 0x00  1ms TH0 = 0xFC  TL0 = 0x66 */
	/*晶振是 12MHz     ，定时时间为：10ms TH0 = 0xD8  TL0 = 0xF0  1ms TH0 = 0xFC  TL0 = 0x18 */
	TH0 = 0xD8; 
	TL0 = 0xF0;
	TR0 = 1;
	ET0 = 1;
	EA = 1;
}
void Time0_Isr(void) interrupt 1
{
	static unsigned int t = 0;
	EA = 0;
	TH0 = 0xD8;
	TL0 = 0xF0;
	t ++;
	os_task_delayms_handle();
	if(t >= 100)
	{
		t=0;
		task_info[OS_GetCurTask()].stack_top = SP;
		task_info[OS_GetCurTask()].status = READY_STATUS;
//		OS_SetCurTask(OS_GetNextTask());
//		SP = task_info[OS_GetCurTask()].stack_top;
		task_info[OS_GetNextTask()].status = RUN_STATUS;
		SP = task_info[OS_GetCurTask()].stack_top;
	}
	EA = 1;
	//加入下面的一行的目的，是让编译器将压栈寄存器全部压栈
	__asm 
}



//系统自带空闲任务
void task_nothing(void)
{
	while(1);
}


//系统初始化
//空闲任务防止其他任务都在等待时使用
void os_init(void)
{
	os_task_init();
	os_task_add(3,task_nothing);
}


