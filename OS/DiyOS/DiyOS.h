#ifndef __DIYOS_H__
#define __DIYOS_H__

#include <reg52.h>
#include <string.h>
#include "type.h"
#include "DiyOSConfig.h"


//系统管理信息结构体
typedef struct
{
	u8 cur_task;
}DIYOS_INFO;

void OS_SetCurTask(u8 curtask);
u8 OS_GetCurTask(void);


typedef struct
{
	u8 addr_tab[TASK_STACK_SIZE];//本任务的栈空间
	u8 stack_top;//本任务的栈顶空间
	u8 status;//任务状态
	u8 num;//本任务的任务号
	u16 delayms; //剩余时间片时间
}DIYTASK_INFO;

//任务状态定义
#define RUN_STATUS		0
#define READY_STATUS	1
#define DELAY_STATUS 	2


void os_task_init(void);
void os_task_add(u8 task_num,void (*task_fun)(void));

//输出栈指针
u8 os_task_outSp(u8 task_num);



//任务调度函数
//void os_task_sch(u8 task_next_index);
void os_task_sch(); //新代码加入延时调度，则由系统去决定哪一个执行

//任务函数启动，可以启动所需要的任务开始
void os_task_start(u8 task_num);
void os_delayms(u16 ms);

/******************  定时器部分 ****************************/
void Time0_Init(void);


/******************  系统初始化 ****************************/

void os_init(void);


#endif

